---
layout: markdown_page
title: GCP Marketplace license
description: "GCP Marketplace ticket request"
category: GitLab Self-Managed licenses
---

{:.no_toc}

----

## GCP Marketplace License Overview

Customer can purchase [GitLab subscription from GCP Marketplace](https://about.gitlab.com/partners/technology-partners/google-cloud-platform/). This purchase allows the customer to pay for the subscription monthly. 

Even though, the customer is paying for the subscription monthly, they signed an annual or multi-year contract with monthly payments.
So they cannot reduce the number of User mid-term. They can only reduce at renewal time - which would mean churn.

The customer purchased via GCP, which makes them a reseller customer. They will not have access to any purchase options in their customer portal accounts and there is no way to add more seats from GCP either. See [GitLab issue 4930](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/4930). To make a change to the subscription the customer would need to reach out to the Sales team. 

- [Example ticket request to reduce seats](https://gitlab.zendesk.com/agent/tickets/380953)
- [Example ticket request to add seats](https://gitlab.zendesk.com/agent/tickets/331091)